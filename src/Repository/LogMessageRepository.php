<?php

namespace Drupal\drupamonitor\Repository;

use Drupal\drupamonitor\Core\Repository;
use Drupal\drupamonitor\Model\LogMessageCollection;
use Drupal\drupamonitor\Model\LogMessage;
use LessQL\Row;

class LogMessageRepository extends Repository
{   
    public function getTable()
    {
        return 'watchdog';
    }
    
    public function getTargetCollection()
    {
        return 'Drupal\drupamonitor\Model\LogMessageCollection';
    }
    
    public function mapRowToEntity( Row $row )
    {
        $variablesArray =  unserialize (   $row->variables );
        $message = $row->message;
        if( is_array(  $variablesArray ) )
        {
            foreach( $variablesArray as $varName => $varValue )
            {
                $message = str_replace($varName, $varValue, $message);
            }
        }
        return new LogMessage($row->wid, $row->type, $message, $row->timestamp, $row->severity);
    }
    
    public function findAll()
    {
        $logMessageCollection = new LogMessageCollection;
        foreach($this->connection->watchdog()->fetchAll() as $row)
        {
           $logMessage = $this->mapRowToEntity( $row );
           $logMessageCollection->add( $logMessage );
        }
        return $logMessageCollection;
    }
}
