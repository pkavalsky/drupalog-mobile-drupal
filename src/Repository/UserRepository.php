<?php

namespace Drupal\drupamonitor\Repository;

use Drupal\drupamonitor\Core\Repository;
use Drupal\drupamonitor\Core\Settings;
use Drupal\drupamonitor\Model\User;
use LessQL\Row;

class UserRepository extends Repository
{
    public function findByNameAndPass( $name, $pass )
    {
        $uid = user_authenticate($name, $pass);
        return $this->find($uid);
    }
    
    public function findByUidAndName( $uid, $name )
    {
        
        $userRow = $this->connection->users()
                                    ->where('uid', $uid)
                                    ->where('name', $name)
                                    ->fetch();

        if( is_null( $userRow ))                        
        {
            return false;
        }
        return $this->mapRowToEntity( $userRow );
    }
    
    public function find( $uid )
    {
        $userRow = $this->connection->users()
                                    ->where('uid', $uid)
                                    ->fetch();
        if( is_null( $userRow ))                        
        {
            return false;
        }
        return $this->mapRowToEntity( $userRow );
    }
    
    private function findIsAllowed( User $user  )
    {
        $uid = $user->getUid();
        $query = 
        "SELECT
        role.rid,
        users.uid,
        role_permission.permission
        FROM 
        users
        JOIN
        users_roles
        ON
        users_roles.uid = users.uid
        JOIN
        role
        ON
        role.rid = users_roles.rid
        JOIN
        role_permission
        ON
        role_permission.rid = role.rid
        WHERE 
        role_permission.module = '".Settings::MODULE_NAME."' AND users.uid = '$uid' AND role_permission.permission = '".Settings::PERMISSION_NAME."'";
        $result = $this->connection->query( $query );
        if( $result->rowCount() > 0 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function getTargetCollection()
    {
        return 'Drupal\drupamonitor\Model\UserCollection';
    }
    
    public function mapRowToEntity( Row $row )
    {
       $user = new User;
       $user->setUid( $row->uid );
       $user->setName( $row->name );
       $user->setIsAllowed( $this->findIsAllowed($user) );
       return $user;
    }
    
    public function getTable()
    {
        return 'users';
    }
    
}
