<?php

namespace Drupal\drupamonitor\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\drupamonitor\Repository\LogMessageRepository;
use Drupal\drupamonitor\Core\Paginator;
use Drupal\drupamonitor\Core\AuthenticatedRouteController;
use Drupal\drupamonitor\Core\ErrorCollection;
use Drupal\drupamonitor\Core\Error;
use Drupal\drupamonitor\Exception\BadRequestException;
use Drupal\drupamonitor\Exception\NotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Drupal\drupamonitor\Utils\MetadataAdapter;

class LogmessageController extends AuthenticatedRouteController
{
    
    
    /**
     * /drupamonitor/api/logmessage/list?page=1&per_page=5&
     */
    public function listAction( Request $request )
    {
        $errorCollection = new ErrorCollection;
        if( null === $request->get('page') )
        {
            $errorCollection->add ( new Error( Response::HTTP_BAD_REQUEST, 'Missing page parameter' ) );
        }
        if(null === $request->get('per_page') )
        {
            $errorCollection->add ( new Error( Response::HTTP_BAD_REQUEST, 'Missing page parameter' ) );
        }
        if( $errorCollection->getTotal() > 0 )
        {
            throw new BadRequestException( $errorCollection  );
        }
        $currentPage = $request->get('page');
        $perPage = $request->get('per_page');
        $logMessageRepository = new LogMessageRepository;
        $paginator = new Paginator( $logMessageRepository  );
        $logMessageCollection = $paginator->paginate( $currentPage , $perPage, 'wid', 'desc');
        $metadataAdapter = new MetadataAdapter( $paginator, $request );
        $response = new JsonResponse;
        if( (int)$paginator->getPageCount() < (int)$currentPage  )
        {
            throw new NotFoundException( new Error( Response::HTTP_NOT_FOUND, 'Max page reached' ) );
        }
        $response->setData([
            '_metadata' =>  $metadataAdapter->getMetadata(),
            'data' => $logMessageCollection->getLogMessagesAsArray()
        ])->send();
    }
    
    /**
     * /drupamonitor/api/logmessage/all
     */
    public function allAction(Request $request)
    {
        $logMessageRepository = new LogMessageRepository();
        $logMessageCollection = $logMessageRepository->findAll();
        $response = new JsonResponse;
        $response->setData([
            'data' => $logMessageCollection->getLogMessagesAsArray()
        ])->send();
    }
}
