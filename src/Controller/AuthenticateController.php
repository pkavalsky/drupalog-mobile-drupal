<?php

namespace Drupal\drupamonitor\Controller;

use Symfony\Component\HttpFoundation\Request;
use Drupal\drupamonitor\Core\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\drupamonitor\Exception\BadRequestException;
use Drupal\drupamonitor\Exception\WrongFormatException;
use Drupal\drupamonitor\Core\Error;
use Drupal\drupamonitor\Security\ServerKey;
use Drupal\drupamonitor\Core\Settings;
use Drupal\drupamonitor\Security\Authenticator;
use Drupal\drupamonitor\Core\UserInterface;
use Firebase\JWT\JWT;

class AuthenticateController extends Controller
{   
    public function loginAction( Request $request )
    {
        if( ! $request->isMethod('POST') )
        {
            throw new BadRequestException( new Error( Response::HTTP_BAD_REQUEST, 'Only POST allowed for this endpoint' ) );
        }
        if( null === $request->get('name')  || null === $request->get('pass')  ||
            empty( $request->get('name') ) || empty( $request->get('pass') ) )
        {
            throw new WrongFormatException( new Error( Response::HTTP_BAD_REQUEST, 'Missing username or password' ) );
        }
        $authenticator = new Authenticator;
        $user = $authenticator->getUser( $request->get('name'), $request->get('pass') );
        if (! $user instanceof UserInterface)
        {
            throw new UnauthorizedException( new Error( Response::HTTP_FORBIDDEN, 'Incorrect username or password' ) );
        }
        $key = ServerKey::getServerKey();
        $jwt = JWT::encode([
            'uid' => $user->getUid(),
            'name' => $user->getName()
        ], $key, Settings::JWT_ENCODING_ALGORITHM );
        $response = new JsonResponse;
        $response->setData([
            'data' => [
                ['jwt' => $jwt]
            ]
        ])->send();
    }
}
