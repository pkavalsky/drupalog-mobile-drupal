<?php

namespace Drupal\drupamonitor\Model;

use Drupal\drupamonitor\Core\UserInterface;
use Drupal\drupamonitor\Core\Entity;

class User extends Entity implements UserInterface
{
    private $uid;
    private $name;
    private $isAllowed;
    
    public function getUid()
    {
        return $this->uid;
    }

    public function getName()
    {
        return $this->name;
    }
    
    public function getIsAllowed()
    {
        return $this->isAllowed;
    }

    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function setIsAllowed( $isAllowed )
    {
        $this->isAllowed = $isAllowed;
    }
    
}

