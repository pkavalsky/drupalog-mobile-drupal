<?php

namespace Drupal\drupamonitor\Model;

use Drupal\drupamonitor\Core\Collection;

class LogMessageCollection extends Collection
{
    public function targetClass()
    {
        return 'Drupal\drupamonitor\Model\LogMessage';
    }
    
    public function getLogMessagesAsArray()
    {
        $logMessagesDataArray = array();
        while($this->valid())
        {
            $logMessagesDataArray[] = $this->next()->getLogMessageData();
        }
        return $logMessagesDataArray;
    }
}
