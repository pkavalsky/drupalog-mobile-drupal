<?php

namespace Drupal\drupamonitor\Model;

use Drupal\drupamonitor\Core\Entity;

class LogMessage extends Entity
{
    private $wid;
    private $type;
    private $message;
    private $severity;
    private $timestamp;
    
    public function __construct($wid, $type, $message, $timestamp, $severity)
    {
        $this->wid = $wid;
        $this->type = $type;
        $this->message = $message;
        $this->timestamp = $timestamp;
        $this->severity = $severity;
    }
    
    public function getSeverity()
    {
        return $this->severity;
    }

    public function getTimestamp()
    {
        return $this->timestamp;
    }

    public function getWid()
    {
        return $this->wid;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getMessage()
    {
        return $this->message;
    }
    
    public function getLogMessageData()
    {
        return [
            'wid' => $this->wid,
            'type' => $this->type,
            'message' => $this->message,
            'timestamp' => $this->timestamp,
            'severity' => $this->severity
        ];
    }


    
    
}
