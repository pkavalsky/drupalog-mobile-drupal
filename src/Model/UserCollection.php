<?php

namespace Drupal\drupamonitor\Model;

use Drupal\drupamonitor\Core\Collection;

class UserCollection extends Collection
{
    public function targetClass()
    {
        return 'Drupal\drupamonitor\Model\User';
    }
}
