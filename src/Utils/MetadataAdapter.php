<?php

namespace Drupal\drupamonitor\Utils;

use Symfony\Component\HttpFoundation\Request;
use Drupal\drupamonitor\Core\Paginator;
use Drupal\drupamonitor\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Response;
use Drupal\drupamonitor\Core\Error;

class MetadataAdapter
{
    private $paginator;
    private $request;
    
    public function __construct(Paginator $paginator, Request $request)
    {
        if( null === $paginator->getCurrent() ||
            null === $paginator->getPerPage() )
        {
            throw new RuntimeException( new Error ( Response::HTTP_INTERNAL_SERVER_ERROR, 'Fatal error: pagination class Drupal\drupamonitor\Core\Paginator doesn\'t have mandatory parameters' ) );
        }
        $this->paginator = $paginator;
        $this->request = $request;
    }
    
    public function getMetadata()
    {
        return [
            'page' => $this->paginator->getCurrent(),
            'per_page' => $this->paginator->getPerPage(),
            'page_count' => $this->paginator->getPageCount( $this->paginator->getPerPage() ),
            'total_count' => $this->paginator->getTotal(),
            'links' => $this->getLinksData()
        ];
    }
    
    private function getLinksData()
    {
        $baseUrl = $this->request->getScheme() . '://' . $this->request->getHttpHost() . $this->request->getPathInfo();
        $perPage = $this->paginator->getPerPage();
        $currentPage = $this->paginator->getCurrent();
        $pageCount = $this->paginator->getPageCount();
        $linksData = [
            'self' => $baseUrl . $this->request->getBaseUrl() . $this->getQueryString($currentPage),
            'first' => $baseUrl . $this->getQueryString(1, $perPage),
        ];
        if( ( (int)$currentPage - 1 ) > 0 )
        {
            $linksData['previous'] = $baseUrl . $this->getQueryString($currentPage - 1);
        }
        if( ( (int)$currentPage +1  ) < (int)$pageCount  )
        {
            $linksData['next'] = $baseUrl . $this->getQueryString($currentPage + 1);
        }
        $linksData['last'] = $baseUrl . $this->getQueryString( $this->paginator->getPageCount() );
        return $linksData;
    }
    
    private function getQueryString( $page )
    {
        return "?page=$page&per_page=".$this->paginator->getPerPage()."&";
    }
}
