<?php

namespace Drupal\drupamonitor\Security;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Drupal\drupamonitor\Exception\UnauthorizedException;
use Drupal\drupamonitor\Core\Error;
use Drupal\drupamonitor\Core\AuthenticatorInterface;
use Drupal\drupamonitor\Security\ServerKey;
use Drupal\drupamonitor\Core\Settings;
use Drupal\drupamonitor\Repository\UserRepository;
use Drupal\drupamonitor\Exception\BadRequestException;
use Drupal\drupamonitor\Core\UserInterface;
use Firebase\JWT\JWT;

class Authenticator implements AuthenticatorInterface
{       
    public function getCredentials( Request $request )
    {
        $authHeaderValue = $request->headers->get('authorization');
        $jwt = explode( ' ', $authHeaderValue );
        if( 
            ! is_array ( $jwt ) ||
            count ( $jwt ) < 2 ||
            strlen( $jwt[1] ) < 15  ||
            $jwt[0] !== 'Bearer' ||
            count ( explode ( '.', $jwt[1] ) ) < 3
        )
        {
            throw new UnauthorizedException( new Error( Response::HTTP_UNAUTHORIZED, 'Wrong authorization header format' ) );
        }
        return $jwt;
    }
    
    public function checkCredentials( $credentials )
    {
        $jwt = $credentials;
        $key = ServerKey::getServerKey();
        $payload = (array)JWT::decode( $jwt[1], $key, [Settings::JWT_ENCODING_ALGORITHM] );
        if(   count( $payload ) < 2 ||
            ! isset ( $payload['name'] ) || 
            ! isset ( $payload['uid'] ) ) 
        {
            throw new BadRequestException( new Error( Response::HTTP_BAD_REQUEST, 'Missing mandatory data in authorization header' ) );
        }
        $userRepository = new UserRepository;
        $user = $userRepository->findByUidAndName( $payload['uid'], $payload['name'] );
        if( !$user instanceof UserInterface )
        {
            throw new UnauthorizedException( new Error( Response::HTTP_FORBIDDEN, 'Incorrect username or password' ) );
        }
        return $user;
    }
    
    public function checkUser( UserInterface $user)
    {
        if( ! $user->getIsAllowed() )
        {
           throw new UnauthorizedException( new Error( Response::HTTP_UNAUTHORIZED, 'Missing required Drupamonitor permission' ) );
        }
    }
    
    public function getUser( $name, $pass )
    {
        $uid = user_authenticate( $name, $pass );
        if( $uid < 1  )
        {
            return false;
        }
        $userRepository = new UserRepository;
        $user = $userRepository->findByUidAndName( $uid, $name );
        if( !$user instanceof UserInterface )
        {
            return false;
        }
        return $user;
    }
}
