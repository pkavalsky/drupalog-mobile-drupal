<?php

namespace Drupal\drupamonitor\Security;

use Drupal\drupamonitor\Core\Settings;

class ServerKey
{
    const LENGTH = 1024;
    
    public static function generate()
    {
        $randomBytes = openssl_random_pseudo_bytes( self::LENGTH );
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $result = '';
        for ($i = 0; $i < self::LENGTH ; $i++)
        {
            $result .= $characters[ord($randomBytes[$i]) % $charactersLength];
        }
        return $result;
    }
    
    public static function getServerKey()
    {
        $key =  Settings::getSetting( Settings::SERVER_KEY_SETTING_NAME  );
        if( is_null ( $key ) )
        {
            $key = self::generate();
            Settings::setSetting( Settings::SERVER_KEY_SETTING_NAME, $key );
        }
        return $key;
    }
}
