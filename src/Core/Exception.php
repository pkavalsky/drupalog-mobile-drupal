<?php

namespace Drupal\drupamonitor\Core;

use Drupal\drupamonitor\Core\ErrorCollection;
use Drupal\drupamonitor\Core\Error;

abstract class Exception extends \Exception implements ExceptionInterface
{
    private $errors;
    
    public function __construct($errors,  \Exception $previous = null )
    {
        if($errors instanceof ErrorCollection)
        {
            $this->addErrors ($errors);
        }
        else if($errors instanceof Error)
        {
            $this->addError($errors);
        }
        parent::__construct("", 0, $previous);
    }
    
    public function addErrors(ErrorCollection $errors)
    {
        $this->errors = $errors;
    }
    
    public function addError(Error $error)
    {
        if(!$this->errors instanceof ErrorCollection)
        {
            $this->errors = new ErrorCollection;
        }
        $this->errors->add($error);
    }
    
    public function getErrors()
    {
        return $this->errors;
    }
}
