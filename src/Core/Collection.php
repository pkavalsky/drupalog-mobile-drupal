<?php

namespace Drupal\drupamonitor\Core;

use Drupal\drupamonitor\Core\Entity;
use Iterator;

abstract class Collection implements Iterator
{
    protected $_total = 0;
    private $_pointer = 0;
    protected $_objects = array();
    
    /**
     * adds object into objects array
     * 
     * @param Entity $object
     * @throws Exception
     */
    public function add($object)
    {
        $class = $this->targetClass();
        if(! ($object instanceof $class))
        {
            throw new \Exception("Wrong input to collection $class");
        }
        $this->_objects[$this->_total] = $object;
        $this->_total++;
    }
    
    abstract function targetClass();
    
    /**
     * move cursor to beginning of the list
     */
    public function rewind()
    {
        $this->_pointer = 0;
    }
    
    /**
     * return object from the current cursor position
     * 
     * @return type
     */
    public function current()
    {
        if(isset($this->_objects[$this->_pointer]))
            return $this->_objects[$this->_pointer];
        return null;
    }
    
    /**
     * return current cursor position
     * 
     * @return type
     */
    public function key()
    {
        return $this->_pointer;
    }
    
    /**
     * return object from the current cursor position and move cursor to next
     * 
     * @return type
     */
    public function next()
    {
        $object = $this->current();
        $class = $this->targetClass();
        if($object instanceof $class)
        {
            $this->_pointer++;
        }
        return $object;
    }
    
    /**
     * confirm that that object exists on the current pointer position
     * 
     * @return type
     */
    public function valid()
    {
        return (! is_null($this->current()));
    }
    
    /**
     * move pointer to desired position
     * 
     * @param type $pointer
     */
    public function move($pointer)
    {
        $this->_pointer = $pointer;
    }
    
    public function getTotal()
    {
        return $this->_total;
    }
}
