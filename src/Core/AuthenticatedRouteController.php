<?php

namespace Drupal\drupamonitor\Core;

abstract class AuthenticatedRouteController extends Controller
{
    protected $user;
    
    public function setUser( UserInterface $user)
    {
        $this->user = $user;
    }
}
