<?php

namespace Drupal\drupamonitor\Core;

class Settings
{
    const PERMISSION_NAME = 'access Drupamonitor';
    const SERVER_KEY_SETTING_NAME = 'drupamonitor_jwt_private_key';
    const JWT_ENCODING_ALGORITHM = 'HS512';
    const MODULE_NAME = 'drupamonitor';
    
    public static function getSetting( $settingName  )
    {
        return variable_get ( $settingName );
    }
    
    public static function setSetting (  $settingName, $value )
    {
        variable_set( $settingName,  $value );
    }
}
