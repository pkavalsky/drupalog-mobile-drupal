<?php

namespace Drupal\drupamonitor\Core;

use Drupal\drupamonitor\Exception\NotFoundException;
use Drupal\drupamonitor\Core\Error;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Drupal\drupamonitor\Core\AuthenticatedRouteController;
use Exception;

class Dispatcher
{
    public static function run($controller, $action)
    {
        try
        {
            $controllerName = 'Drupal\drupamonitor\Controller\\' . ucfirst( strtolower( $controller ) ) . 'Controller';
            $actionName = strtolower( $action ) . 'Action';
            if (! class_exists( $controllerName ) ||
                ! method_exists($controllerName, $actionName ) )
            {
                throw new NotFoundException( new Error( Response::HTTP_NOT_FOUND, 'Not found' ) );
            }
            $request = Request::createFromGlobals();
            $controllerObject = new $controllerName;
            if( $controllerObject instanceof AuthenticatedRouteController )
            {
                $authenticatorClassName = 'Drupal\drupamonitor\Security\Authenticator';
                if( !class_exists($authenticatorClassName) )
                {
                    throw new Exception('Authenticator class was not defined and controller: ' . $controllerName . ' required it');
                }
                $authenticator = new $authenticatorClassName;
                $credentials = $authenticator->getCredentials( $request );
                $user = $authenticator->checkCredentials( $credentials );
                $authenticator->checkUser( $user );
            }
            if( isset( $user ) && $user instanceof UserInterface )
            {
                $controllerObject->setUser( $user );
            }
            $controllerObject->setActionName( $actionName );
            $controllerObject->setRequest( $request );
            $controllerObject->beforeExecuteRoute();
            $controllerObject->{$actionName}($request);
        } 
        catch (Exception $ex) 
        {
            $exceptionHandler = new ExceptionHandler;
            $exceptionHandler->render($ex);
        }
        exit();
    }
}
