<?php

namespace Drupal\drupamonitor\Core;

use LessQL\Row;

interface RepositoryInterface
{
    public function getTable();
    
    public function mapRowToEntity(Row $row);
    
    public function getTargetCollection();
}
