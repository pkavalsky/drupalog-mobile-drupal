<?php

namespace Drupal\drupamonitor\Core;

use Symfony\Component\HttpFoundation\JsonResponse;

interface ExceptionInterface
{
    public function handle( JsonResponse $response, ErrorCollection $errorCollection );
    
    public function addError( Error $error );
    
    public function addErrors( ErrorCollection $errorCollection );
    
    public function getErrors();
}
