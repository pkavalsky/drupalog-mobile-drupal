<?php

namespace Drupal\drupamonitor\Core;

class Error
{
    private $code;
    private $message;
    
    public function __construct( $code, $message )
    {
        $this->code = $code;
        $this->message = $message;
    }
    
    public function getCode()
    {
        return $this->code;
    }
    
    public function getMessage()
    {
        return $this->message;
    }
    
    public function getErrorData()
    {
        return [
            'code' => $this->code,
            'message' => $this->message
        ];
    }
}
