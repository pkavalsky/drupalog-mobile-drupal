<?php

namespace Drupal\drupamonitor\Core;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Drupal\drupamonitor\Core\ExceptionInterface;
use Drupal\drupamonitor\Core\ErrorCollection;
use Drupal\drupamonitor\Core\Error;
use Exception;

class ExceptionHandler
{
    public function render( Exception $exception )
    {
        $response = new JsonResponse();
        if ($exception instanceof ExceptionInterface)
        {
            $errorCollection = $exception->getErrors();
        }
        else 
        {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            $errorCollection = new ErrorCollection;
            $errorCollection->add( new Error( 500, $exception->getMessage() ) );
        }
        $response->setData([
                'errors' =>  $errorCollection->getErrorsAsArray() 
        ]);
        if( $exception instanceof ExceptionInterface)
        {
            $exception->handle( $response,  $errorCollection);
        }
        $response->send();
    }
}
