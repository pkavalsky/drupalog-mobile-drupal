<?php

namespace Drupal\drupamonitor\Core;

use Symfony\Component\HttpFoundation\Request;

class Controller
{
    protected $actionName;
    protected $request;
    
    public function setActionName ( string $actionName )
    {
        $this->actionName = $actionName;
    }
    
    public function setRequest( Request $request )
    {
        $this->request = $request;
    }

    public function beforeExecuteRoute() { }
}
