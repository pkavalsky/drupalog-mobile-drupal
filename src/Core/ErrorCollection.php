<?php

namespace Drupal\drupamonitor\Core;

class ErrorCollection extends Collection
{
    public function targetClass()
    {
        return 'Drupal\drupamonitor\Core\Error';
    }
    
    public function getErrorsAsArray()
    {
        $errorDataArray = array();
        while($this->valid())
        {
            $errorDataArray[] = $this->next()->getErrorData();
        }
        return $errorDataArray;
    }
}
