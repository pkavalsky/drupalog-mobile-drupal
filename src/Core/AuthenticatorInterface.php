<?php

namespace Drupal\drupamonitor\Core;

use Symfony\Component\HttpFoundation\Request;
use Drupal\drupamonitor\Core\UserInterface;

interface AuthenticatorInterface
{
    public function getCredentials( Request $request );
    
    public function checkCredentials( $credentials );
    
    public function checkUser( UserInterface $user );
}
