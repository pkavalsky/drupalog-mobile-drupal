<?php

namespace Drupal\drupamonitor\Core;

abstract class Repository implements RepositoryInterface
{
    protected $connection;
    
    public function __construct()
    {
        $drupalPdoConnection = \Database::getConnection();
        $this->connection = new \LessQL\Database( $drupalPdoConnection );
        /*
         * set primary keys
         */
        $this->connection->setPrimary('users', 'uid');
        $this->connection->setPrimary('watchdog', 'wid');
    }
    
    public function getConnection()
    {
        return $this->connection;
    }
    
    public function getTotal()
    {
        $tableName = $this->getTable();
        return $this->connection->{$tableName}()->count( '*' );
    }
}
