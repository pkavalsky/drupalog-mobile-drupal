<?php

namespace Drupal\drupamonitor\Core;

use Drupal\drupamonitor\Core\RepositoryInterface;
use Drupal\drupamonitor\Exception\LogicException;
use Symfony\Component\HttpFoundation\Response;
use Drupal\drupamonitor\Core\ErrorCollection;
use Drupal\drupamonitor\Core\Error;

class Paginator
{
    protected $current;
    protected $perPage;
    protected $total;
    protected $pageCount;
    
    public function __construct( RepositoryInterface $repository )
    {
        $this->repository = $repository;
    }
    
    public function paginate( $currentPage, $perPage, $orderColumn, $order )
    {
        $this->current = $currentPage;
        $this->perPage = $perPage;
        $offset = ( $currentPage * $perPage ) - $perPage;
        $limit = $perPage;
        $this->validateParams($currentPage, $perPage, $orderColumn, $order);
        $rows = $this->repository
             ->getConnection()
             ->{$this->repository->getTable()}()
             ->limit($limit, $offset)
             ->orderBy($orderColumn, $order)
             ->fetchAll();
        $collectionName = $this->repository->getTargetCollection();
        $collection = new $collectionName;
        foreach( $rows as $row )
        {
            $collection->add ( $this->repository->mapRowToEntity( $row ) );
        }
        return $collection;
    }
    
    private function validateParams( $currentPage, $perPage, $orderColumn, $order )
    {
        $errorCollection = new ErrorCollection();
        if( $currentPage < 1 )
        {
            $errorCollection->add( new Error( Response::HTTP_BAD_REQUEST, 'current page needs to be int bigger than 0') );
        }
        if( $perPage < 1 )
        {
            $errorCollection->add( new Error( Response::HTTP_BAD_REQUEST, 'per page needs to be int bigger than 0') );
        }
        if( $order !== 'desc' && 
            $order !== 'asc' )
        {
             $errorCollection->add( new Error( Response::HTTP_BAD_REQUEST, 'order can only be desc or asc') );
        }
        if( $errorCollection->getTotal() > 0)
        {
            throw new LogicException( $errorCollection );
        }
    }
    
    public function getTotal()
    {
        if( ! isset(  $this->total ) )
        {
            $this->total =  $this->repository->getTotal();
            return $this->total;
        }
        return $this->total;
    }
    
    public function getPageCount()
    {
        if( ! isset(  $this->pageCount ) )
        {
            $this->pageCount =  ceil(  $this->repository->getTotal() / $this->perPage );
            return $this->pageCount;
        }
        return $this->pageCount;
    }
    
    public function getNext()
    {
        $pageCount = $this->getPageCount();
        if( (int)$this->current < (int)$pageCount )
        {
            return $this->current + 1;
        }
        return false;
    }
    
    public function getCurrent()
    {
        return $this->current;
    }
    
    public function getPerPage()
    {
        return $this->perPage;
    }
}
