<?php

namespace Drupal\drupamonitor\Exception;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Drupal\drupamonitor\Core\ErrorCollection;
use Drupal\drupamonitor\Core\Exception;

class UnauthorizedException extends Exception 
{
    public function handle(JsonResponse $response, ErrorCollection $errorCollection)
    {
        $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
    }
}
