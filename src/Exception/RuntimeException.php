<?php

namespace Drupal\drupamonitor\Exception;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Drupal\drupamonitor\Core\ErrorCollection;
use Drupal\drupamonitor\Core\Exception;

class RuntimeException extends Exception 
{
    public function handle(JsonResponse $response, ErrorCollection $errorCollection)
    {
        $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
